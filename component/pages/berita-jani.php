<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/dayjs@1/dayjs.min.js"></script>
<style>
    body {
        background-color: #ffe7b524;
    }
    .navbar.fixed-top{
	    border-top: 14px solid #865f3a;
    }
    .card-title {
        background: #9f7f61;
        padding: 10px 0px;
        border-radius: 50px;
        color: white;
        font-style: italic;
        font-weight: bold;
    }
    .mb-1.berita-truncate-1.text-primary small {
        background: #29824c;
        padding: 10px 10px;
        color: white;
        font-style: italic;
    }
    a {
        color: rgba(var(--bs-primary-rgb), var(--bs-link-opacity, 1));
        text-decoration: none;
    }
    .video-yutup-jani {
        width : 100%;
        min-height : 400px;
    }
    .tabel-emas-jani {
        border:1px solid #b3adad;
        border-collapse:collapse;
        padding:5px;
        margin: auto;
    }
    .tabel-emas-jani th {
        border:1px solid #b3adad;
        padding:5px;
        background: #f0f0f0;
        color: #313030;
    }
    .tabel-emas-jani td {
        border:1px solid #b3adad;
        text-align:left;
        padding:5px;
        background: #ffffff;
        color: #313030;
    }

    .twitter { background: #55acee; }
    .facebook { background: #3B5998; }
    .linkedin    { background: #4875B4; }
    .link { background: rgba(var(--bs-danger-rgb), var(--bs-link-opacity, 1)); }

    .share:hover {
        background-color: rgba(var(--bs-primary-rgb), var(--bs-link-opacity, 1)) !important;
        opacity: .6 !important;
    }

    .form-control{
        min-height: 50px;
        border-radius: 12px;
    }

    .form-control:focus {
        color: #495057;
        border-color: #35b69f;
        outline: 0;
        box-shadow: none;
        text-indent: 10px;
        transition: 0.3s ease-in-out;
    }

    .comment-text{
        font-size: 13px;
    }

</style>
<div class="">
    <h1 class="h2 fw-bold">Harga Emas Terbaru Antam 29 Juli 2023, Termurah Rp 585.500 dan Termahal Rp 1.011.600.000</h1>
    <div class="row align-items-center">
        <div class="col-md-4">
            <small>Dipublish pada <i>Sabtu, 29 Juli 2023</i></small>
        </div>
        <div class="col-md-8 d-flex justify-content-evenly align-items-center mt-2">
            <span class="d-inline d-md-none">Share</span> <span class="d-none d-md-inline">Share Artikel ini!</span>
            <a href="#" onclick="return gabisa();" class="px-4 share twitter btn text-white">
                <i class="fa fa-twitter"></i>
            </a>
            <a href="#" onclick="return gabisa();" class="px-4 share facebook btn text-white">
                <i class="fa fa-facebook"></i>
            </a>
            <a href="#" onclick="return gabisa();" class="px-4 share linkedin btn text-white">
                <i class="fa fa-linkedin"></i>
            </a>
            <a href="#" onclick="return copyLink();" class="px-4 share link btn text-white">
                <i class="fa fa-copy"></i> <span class="d-none d-md-inline"> Copy Link</span>
            </a>
        </div>
    </div>
    <hr>
    <iframe class="video-yutup-jani mb-3" src="https://www.youtube.com/embed/dD3mgLd1JYk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
    <hr>
    <div>
        <p><b><a href="http://liputan6.com" target="_blank">Liputan6.com</a></b>, Jakarta - Harga emas yang dijual PT Aneka Tambang Tbk atau emas Antam naik tipis pada perdagangan Sabtu ini. Harga emas hari ini di Antam naik Rp 3.000 menjadi Rp 1.071.000 per gram dari sebelumnya Rp 1.068.000 per gram.</p>
        <p>Demikian juga dengan harga emas Antam untuk pembelian kembali atau harga emas buyback pada hari ini juga naik dengan nilai yang lebih besar yaitu Rp 4.000. Harga emas Antam untuk buyback menjadi Rp 950.000 per gram dari sebelumnya Rp 946.000 per gram.</p>
        <p>Harga buyback ini merupakan patokan bila Anda menjual emas, maka harga emas Antam akan dihargai Rp 950.000 per gram.</p>
        <p>Melansir laman <b><a href="http://logammulia.com" target="_blank">LogamMulia</a></b>, Sabtu (29/7/2023), Antam menjual emas dengan ukuran mulai 0,5 gram hingga 1.000 gram. Hingga pukul 08.32 WIB, harga emas Antam sebagian besar masih ada.</p>
        <p>Antam juga menawarkan emas seri batik, gift seri dengan ukuran beragam. Terbaru, emas edisi khusus uang dirilis Antam adalah Emas Imlek Rabbit yang keluar di awal tahun ini.</p>
        <p>Harga emas Antam hari ini belum termasuk PPh 22 sebesar 0,9 persen. Anda bisa memperoleh potongan pajak lebih rendah (0,45 persen) jika menyertakan Nomor Pokok Wajib Pajak (NPWP).</p>
        <div class="text-center my-3">
            <h2 class="h4">Berikut Harga Emas per 29 Juli 2023</h2>
            <table class="tabel-emas-jani">
                <thead>
                    <tr>
                        <th>Berat</th>
                        <th>Harga</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>0,5 gram</td>
                        <td>Rp 585.500</td>
                    </tr>
                    <tr>
                        <td>1 gram</td>
                        <td>Rp 1.071.000</td>
                    </tr>
                    <tr>
                        <td>2 gram</td>
                        <td>Rp 2.082.000</td>
                    </tr>
                    <tr>
                        <td>3 gram</td>
                        <td>Rp 3.098.000</td>
                    </tr>
                    <tr>
                        <td>5 gram</td>
                        <td>Rp 5.130.000</td>
                    </tr>
                    <tr>
                        <td>10 gram</td>
                        <td>Rp 10.205.000</td>
                    </tr>
                    <tr>
                        <td>25 gram</td>
                        <td>Rp 25.387.000</td>
                    </tr>
                    <tr>
                        <td>50 gram</td>
                        <td>Rp 50.695.000</td>
                    </tr>
                    <tr>
                        <td>100 gram</td>
                        <td>Rp 101.312.000</td>
                    </tr>
                    <tr>
                        <td>250 gram</td>
                        <td>Rp 253.015.000</td>
                    </tr>
                    <tr>
                        <td>500 gram</td>
                        <td>Rp 505.820.000</td>
                    </tr>
                    <tr>
                        <td>1.000 gram</td>
                        <td>Rp 1.011.600.000</td>
                    </tr>
                </tbody>
            </table>
            <img perbesar src="./assets/images/berita-emas-jani.webp" alt="Foto Emas" class="col-12 col-md-10 mt-3 rounded">
        </div>
        <i class="w-100">Sumber : <a href="https://www.liputan6.com/bisnis/read/5356489/harga-emas-terbaru-antam-29-juli-2023-termurah-rp-585500-dan-termahal-rp-1011600000" target="_blank">Liputan 6 - Harga Emas Terbaru Antam 29 Juli 2023</a></i>
        <p class="mt-3">Upload by : <b>Rafsanjani Hafidh Firmansyah</b>
            <a href="http://linkedin.com/in/rafsanjanihf" target="_blank">
                <svg fill="#134a7b" height="30px" width="30px" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 455 455" xml:space="preserve">
                    <g>
                        <path style="fill-rule:evenodd;clip-rule:evenodd;" d="M246.4,204.35v-0.665c-0.136,0.223-0.324,0.446-0.442,0.665H246.4z"/>
                        <path style="fill-rule:evenodd;clip-rule:evenodd;" d="M0,0v455h455V0H0z M141.522,378.002H74.016V174.906h67.506V378.002z    M107.769,147.186h-0.446C84.678,147.186,70,131.585,70,112.085c0-19.928,15.107-35.087,38.211-35.087   c23.109,0,37.31,15.159,37.752,35.087C145.963,131.585,131.32,147.186,107.769,147.186z M385,378.002h-67.524V269.345   c0-27.291-9.756-45.92-34.195-45.92c-18.664,0-29.755,12.543-34.641,24.693c-1.776,4.34-2.24,10.373-2.24,16.459v113.426h-67.537   c0,0,0.905-184.043,0-203.096H246.4v28.779c8.973-13.807,24.986-33.547,60.856-33.547c44.437,0,77.744,29.02,77.744,91.398V378.002   z"/>
                    </g>
                </svg>
            </a>
            <a href="http://jani.my.id" target="_blank">
                <svg fill="#134a7b" height="30px" width="30px" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 455 455" xml:space="preserve">
                    <path style="fill-rule:evenodd;clip-rule:evenodd;" d="M0,0v455h455V0H0z M227.5,385c-2.515,0-5.011-0.067-7.503-0.184  C136.496,380.902,70,311.968,70,227.5C70,140.512,140.516,70,227.5,70c0.2,0,0.396,0.008,0.596,0.008  c40.097,0.149,76.652,15.28,104.38,40.085c-18.466-12.252-40.054-19.296-63.127-19.296c-37.507,0-71.096,18.606-93.691,47.948  c-17.412,20.554-28.692,50.946-29.46,85.048v7.417c0.768,34.102,12.048,64.49,29.463,85.048  c22.591,29.342,56.18,47.948,93.687,47.948c23.069,0,44.654-7.044,63.115-19.288C304.599,369.837,267.824,385,227.5,385z   M339.288,338.423c-16.125,9.59-34.185,14.65-52.565,14.65c-15.801,0-31.205-3.735-45.321-10.724  c40.051-8.273,70.786-56.539,70.786-114.849c0-58.307-30.732-106.572-70.78-114.848c14.115-6.986,29.518-10.721,45.316-10.721  c18.385,0,36.447,5.06,52.571,14.653C367.536,145.048,385,184.231,385,227.5C385,270.771,367.534,309.956,339.288,338.423z"/>
                </svg>
            </a>
        </p>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-10">
        <div class="card border-0">
            <div class="pt-3">
                <h6>Komentar <code>[Tersimpan di Data API]</code></h6>
            </div>
            <div class="mt-3 form-color d-flex flex-row align-items-start">
                <img src="./assets/images/logo-unsia-only.png" width="50" class="rounded-circle me-2">
                <div class="col">
                    <form id="komenForm" action="" method="post">
                        <input type="text" id="komenNama" class="form-control" maxlength="32" placeholder="Masukkan Nama Anda">
                        <textarea type="text" id="komenText" class="my-2 form-control" rows="4" placeholder="Tulis Komentar"></textarea>
                        <button type="submit" class="btn btn-success">Post!</button>
                    </form>
                </div>
            </div>
            <div class="mt-2">
                <div class="d-flex flex-row py-3 ps-3">
                    <img src="./assets/images/foto-jani.png" width="40" height="40" class="rounded-circle me-3">
                    <div class="w-100">
                        <div class="d-md-flex justify-content-between align-items-center">
                            <div class="d-md-flex flex-row align-items-center align">
                                <span class="fw-bolder me-2 w-100">Jani Jack</span>
                                <small class="badge text-bg-danger">Komentar Penerbit</small>
                            </div>
                        </div>
                        <p class="text-justify comment-text mb-0 mt-2 border p-2 rounded border-gray-300">Horeee Kelar UAS, semoga pelajaran yang diperoleh menjadi bekal untuk masa depan yang lebih gemilang!</p>
                    </div>
                </div>
                <div id="komentarBaru"></div>
            </div>
        </div>
    </div>
</div>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        var detekh5 = document.querySelector('.card-title.text-center');
        var tambahp = document.createElement('p');
        tambahp.className = 'text-center small';
        tambahp.textContent = 'Web Developer | DevOps Engineer';
        detekh5.insertAdjacentElement('afterend', tambahp);
    });
    function gabisa() {
        alert("Maaf belum bisa hehee..");
    }

    function copyLink() {
        const textCopy = '<?php echo (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>';

        navigator.clipboard.writeText(textCopy)
            .then(() => alert(`URL ${textCopy} berhasil disalin ke clipboard.`))
            .catch((err) => alert('Gagal menyalin teks ke clipboard: ' + err));
    }

    $('img[perbesar]').css('cursor', 'pointer').click(function () {
        const src = $(this).attr('src');
        $('<div>').css({
            background: `RGBA(0, 0, 0, 0.5) url(${src}) no-repeat center / contain`,
            width: '100%',
            height: '100%',
            position: 'fixed',
            zIndex: '999999',
            top: '0',
            left: '0',
            cursor: 'zoom-out',
            opacity: '0',
            transition: 'opacity 0.4s ease-in-out',
        }).on('click', function () {
            $(this).remove();
        }).appendTo('body');

        setTimeout(() => $('div').css('opacity', '1'), 20);
    });

    async function fetchAndDisplayComments() {
        try {
            const postURL = "https://pb.terretech.id/api/collections/comment/records";
            const response = await fetch(postURL, {
                method: "GET",
                headers: { "Content-Type": "application/json" },
            });
            const { items: comments } = await response.json();

            const komentarBaru = document.getElementById('komentarBaru');
            comments.forEach((comment, index) => {
                const commentDiv = createCommentElement(comment, index === comments.length - 1);
                komentarBaru.appendChild(commentDiv);
            });
        } catch (error) {
            console.error("Error:", error);
        }
    }

    function createCommentElement(commentData, lastComment) {
        const commentDiv = document.createElement('div');
        const newCommentBadge = lastComment ? '<small class="badge text-bg-primary">New Comment</small>' : '';

        commentDiv.classList.add('d-flex', 'flex-row', 'py-3', 'ps-3', commentData.id);
        commentDiv.innerHTML = `
            <img src="./assets/images/logo-unsia-only.png" width="40" height="40" class="rounded-circle me-3">
            <div class="w-100">
                <div class="d-md-flex justify-content-between align-items-center">
                    <div class="d-md-flex flex-row align-items-center">
                        <span class="fw-bolder me-2 w-100">${commentData.komenNama}</span>
                        ${newCommentBadge}
                    </div>
                    <small>Diupload pada <i>${dayjs(commentData.created).format('DD-MM-YYYY HH:mm')}</i></small>
                </div>
                <p class="text-justify comment-text mb-0 mt-2 border p-2 rounded border-gray-300">${commentData.komenText}</p>
                <span onclick="deleteComment('${commentData.id}')" class="text-danger small" style="cursor:pointer"><i class="fa fa-trash"></i> Delete Komentar</span>
            </div>
        `;

        return commentDiv;
    }

    async function postComment(event) {
        event.preventDefault();

        const komenNamas = document.getElementById('komenNama').value;
        const komenTexts = document.getElementById('komenText').value;
        const postData = { komenNama: komenNamas, komenText: komenTexts };

        const postURL = "https://pb.terretech.id/api/collections/comment/records";

        try {
            await fetch(postURL, {
                method: "POST",
                body: JSON.stringify(postData),
                headers: { "Content-Type": "application/json" },
            });

            document.getElementById('komenNama').value = '';
            document.getElementById('komenText').value = '';

            const komentarBaru = document.getElementById('komentarBaru');
            komentarBaru.innerHTML = '';
            await fetchAndDisplayComments();

            alert("Berhasil Komentar!");
        } catch (error) {
            alert("Error Komen, Maaf..");
            console.error("Error:", error);
        }
    }

    async function deleteComment(id) {
        const postURL = `https://pb.terretech.id/api/collections/comment/records/${id}`;
        
        if (confirm("Apakah Anda yakin ingin menghapus komentar ini?")) {
            try {
                await fetch(postURL, {
                    method: "DELETE",
                    headers: { "Content-Type": "application/json" },
                });
                
                const commentElement = document.querySelector(`.${id}`);
                if (commentElement) {
                    commentElement.remove();
                }
                
                alert('Berhasil Menghapus Komentar');
            } catch (error) {
                console.error("Error:", error);
            }
        } else {
            alert('Penghapusan komentar dibatalkan.');
        }
    }


    window.onload = fetchAndDisplayComments;
    document.getElementById("komenForm").addEventListener("submit", postComment);
</script>