<!-- UAS Pemrograman Web I
Kelompok 4 - IT-204

Nama : Arwan
NIM  : 220401010092
Nama : Rafsanjani Hafidh Firmansyah
NIM  : 220401010090
Nama : Krisna Aji Putra
NIM  : 220401010196
Nama : Iqbal Hadi Nugraha
NIM  : 220401070409 -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home - UAS Pemrograman Web 1 | IT204 - Kelompok 4</title>
    <link rel="shortcut icon" href="assets/images/logo-unsia-only.png" type="image/x-icon">
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Home - UAS Pemrograman Web 1 | IT204 - Kelompok 4" />
    <meta property="og:description" content="IT-204 | Kelompok 4 Yang terdiri dari Arwan, Rafsanjani Hafidh Firmansyah, Krisna Aji Putra, Iqbal Hadi Nugraha" />
    <meta property="og:image" content="<?php echo (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]"; ?>/assets/images/logo-unsia-only.png" />
    <meta property="og:image:width" content="150" />
    <meta property="og:image:height" content="150" />
    <meta property="og:image:type" content="image/png" />
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600;700&family=Work+Sans:ital,wght@0,400;0,500;0,600;0,700;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">
</head>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
<body>
<div id="preloader">
	<div id="status">
        <h4 class="mt-auto" style="margin-bottom: -40px;">Memuat...</h4>
    </div>
</div>
<div>
    <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-white shadow-sm">
        <div class="container py-2">
            <a class="navbar-brand" href="#">
                <img src="https://unsia.ac.id/wp-content/uploads/2022/11/LOGO-UNSIA-1-300x93.png" alt="Logo Unsia" height="42" class="d-inline-block align-text-top">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasNavbar2" aria-controls="offcanvasNavbar2" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="offcanvas offcanvas-end text-bg-light" tabindex="-1" id="offcanvasNavbar2" aria-labelledby="offcanvasNavbar2Label">
                <div class="offcanvas-header">
                    <h5 class="offcanvas-title" id="offcanvasNavbar2Label">Universitas Siber Asia</h5>
                    <button type="button" class="btn-close btn-close-dark" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                </div>
                <div class="offcanvas-body">
                    <ul class="navbar-nav justify-content-start flex-grow-1 pe-3">
                        <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="index.php">Home</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="profile-kami.php">Profile Kami</a>
                        </li>
                    </ul>
                    <div class="d-flex ml-auto align-items-center">
                        <b class="text-primary">IT-204 - Kelompok 4</b>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</div>

<div class="container-fluid" style="padding-top: 140px;">
    <div class="container bg-white shadow pt-md-3 px-md-4 pb-5">
        <h1 class="fw-bold h2 pt-2">UAS Pemrograman Web I - IT 204</h1>
        <div class="row">
            <div class="col-md-8">
                <h3 class="mb-4">Baca Berita</h3>
                <div class="card my-4 border-0">
                    <div class="row g-0">
                        <div class="col-md-4">
                            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT5TtOJjM_G-iw-S8AWRSNxDltQBn6Dwzxc1A&usqp=CAU" alt="picture of sun" class="img-fluid rounded foto-list-berita">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body p-1 p-md-3 px-2 py-3">
                                <small class="d-block d-md-none"><i>Senin, 31 Juli 2023</i></small>
                                <h5 class="card-title mb-0 berita-truncate-1">5 Efek Badai Matahari terhadap Bumi, Termasuk 'Kiamat' Internet</h5>
                                <p class="card-text mb-1">
                                    <small class="d-none d-md-inline"><i>Senin, 31 Juli 2023</i> - </small>
                                    <small class="text-primary"><b>Dibuat Oleh : Arwan</b></small></p>
                                <p class="card-text berita-truncate-2">Jakarta, CNN Indonesia -- Fenomena Badai Matahari memiliki dampak yang berbeda-beda di setiap wilayah Bumi tergantung titik serangan atau kondisi geografisnya, mulai dari tergantungnya alat komunikasi hungga padamnya listrik 'kiamat' internet.</p>
                                <a href="lihat-berita.php?page=5 Efek Badai Matahari terhadap Bumi, Termasuk 'Kiamat' Internet" class="px-3 py-2 bg-primary text-light rounded text-decoration-none">Baca Selengkapnya</a>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="card my-4 border-0">
                    <div class="row g-0">
                        <div class="col-md-4">
                            <img src="./assets/images/berita-emas-jani.webp" class="img-fluid rounded foto-list-berita" alt="Foto Berita">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body p-1 p-md-3 px-2 py-3">
                                <small class="d-block d-md-none"><i>Sabtu, 29 Juli 2023</i></small>
                                <h5 class="card-title mb-0 berita-truncate-1">Harga Emas Terbaru Antam 29 Juli 2023, Termurah Rp 585.500 dan Termahal Rp 1.011.600.000</h5>
                                <p class="card-text mb-1">
                                    <small class="d-none d-md-inline"><i>Sabtu, 29 Juli 2023</i> - </small>
                                    <small class="text-primary"><b>Dibuat Oleh : Rafsanjani Hafidh F.</b></small></p>
                                <p class="card-text berita-truncate-2">Liputan6.com, Jakarta - Harga emas yang dijual PT Aneka Tambang Tbk atau emas Antam naik tipis pada perdagangan Sabtu ini. Harga emas hari ini di Antam naik Rp 3.000 menjadi Rp 1.071.000 per gram dari sebelumnya Rp 1.068.000 per gram.</p>
                                <a href="lihat-berita.php?page=Harga Emas Terbaru Antam 29 Juli 2023, Termurah Rp 585.500 dan Termahal Rp 1.011.600.000" class="px-3 py-2 bg-primary text-light rounded text-decoration-none">Baca Selengkapnya</a>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="card my-4 border-0">
                    <div class="row g-0">
                        <div class="col-md-4">
                            <img src="./assets/images/Artikel-Krisna.jpeg" class="img-fluid rounded foto-list-berita" alt="Foto Berita">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body p-1 p-md-3 px-2 py-3">
                                <small class="d-block d-md-none"><i>Senin, 31 Juli 2023</i></small>
                                <h5 class="card-title mb-0 berita-truncate-1">Prajurit TNI Aktif Boleh Duduki Jabatan Sipil, tapi Saat Korupsi Ogah Tunduk Hukum Sipil</h5>
                                <p class="card-text mb-1">
                                    <small class="d-none d-md-inline"><i>Senin, 31 Juli 2023</i> - </small>
                                    <small class="text-primary"><b>Dibuat Oleh : Krisna Aji Putra</b></small></p>
                                <p class="card-text berita-truncate-2">JAKARTA, KOMPAS.com - Porsi prajurit TNI aktif menduduki jabatan sipil dianggap perlu dievaluasi buntut kisruh penanganan kasus suap yang menyeret nama Kepala Badan Nasional Pencarian dan Pertolongan (Basarnas) Marsekal Madya Henri Alfiandi.</p>
                                <a href="lihat-berita.php?page=Prajurit TNI Aktif Boleh Duduki Jabatan Sipil, tapi Saat Korupsi Ogah Tunduk Hukum Sipil" class="px-3 py-2 bg-primary text-light rounded text-decoration-none">Baca Selengkapnya</a>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="card my-4 border-0">
                    <div class="row g-0">
                        <div class="col-md-4">
                            <img src="https://akcdn.detik.net.id/community/media/visual/2023/08/01/upacara-penutupan-operasi-pencarian-8-penambang-yang-terjebak-air-di-lubang-galian-emas-desa-pancurendang-kecamatan-ajibarang-_43.jpeg?w=700&q=90" class="img-fluid rounded foto-list-berita" alt="Foto Berita">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body p-1 p-md-3 px-2 py-3">
                                <small class="d-block d-md-none"><i>Selasa, 1 Agustus 2023</i></small>
                                <h5 class="card-title mb-0 berita-truncate-1">Pencarian 8 Penambang Terjebak di Banyumas Disetop, Korban Dinyatakan Hilang</h5>
                                <p class="card-text mb-1">
                                    <small class="d-none d-md-inline"><i>Selasa, 1 Agustus 2023</i> - </small>
                                    <small class="text-primary"><b>Dibuat Oleh : Iqbal Hadi Nugraha</b></small></p>
                                <p class="card-text berita-truncate-2">Upacara penutupan operasi pencarian 8 penambang yang terjebak air di lubang galian emas Desa Pancurendang, Kecamatan Ajibarang, Banyumas, Selasa (1/8/2023).</p>
                                <a href="lihat-berita.php?page=Pencarian 8 Penambang Terjebak di Banyumas Disetop, Korban Dinyatakan Hilang" class="px-3 py-2 bg-primary text-light rounded text-decoration-none">Baca Selengkapnya</a>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                
            </div>
            <div class="col-md-4">
                <div class="card w-100">
                <img src="assets/images/logo-unsia.png" class="card-img-top m-auto p-5" alt="Logo Unsia">
                <div class="card-body">
                    <h5 class="card-title text-center">Universitas Siber Asia</h5>
                    <p class="card-text text-center">IT-204 | Kelompok 4</p>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <p class="mb-1 fw-bold">Arwan</p>
                        <p class="mb-1"><small>NIM : 220401010092</small></p>
                    </li>
                    <li class="list-group-item">
                        <p class="mb-1 fw-bold">Rafsanjani Hafidh Firmansyah</p>
                        <p class="mb-1"><small>NIM : 220401010090</small></p>
                    </li>
                    <li class="list-group-item">
                        <p class="mb-1 fw-bold">Krisna Aji Putra</p>
                        <p class="mb-1"><small>NIM : 220401010196</small></p>
                    </li>
                    <li class="list-group-item">
                        <p class="mb-1 fw-bold">Iqbal Hadi Nugraha</p>
                        <p class="mb-1"><small>NIM : 220401070409</small></p>
                    </li>
                    <div class="card-body">
                        <h6 class="fw-bold">Lagu Indonesia Raya</h6>
                        <audio src="assets/audio/lagu-indonesia-raya.mp3" class="w-100" controls="" autoplay loop muted></audio>
                    </div>
                </ul>
            </div>
            </div>
        </div>
    </div>
</div>

<footer class="mt-5">
    <div class="container-fluid shadow-lg" style="border-bottom: 34px solid #134a7b;">
        <div class="container bg-white d-flex justify-content-center p-5">
            <div class="col-md-3">
                <img src="assets/images/logo-unsia.png" alt="Logo Unsia Footer" class="w-100">
            </div>
        </div>
    </div>
</footer>

</body>
    <script src="assets/js/bootstrap.bundle.js"></script>
    <script>
		$(window).on('load', function() { 
			$('#status').delay(600).fadeOut(); 
            $('#preloader').fadeOut('slow'); 
            $('body').delay(600).css({'overflow':'visible'});
		})
    </script>
</html>
